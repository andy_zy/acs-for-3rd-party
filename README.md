#Acceptance Criteria templates for 3rd party analytic integrations in terms of ASD#


##Description##

The main purpose of the integration requirements in the AC format is to figure out the "definition of done" from QC perspective and reduce the scope of tracking responsibilities(fields which have to be set by us in the request).

##How to use it##

Each integration has mandatory and optional fields. First of all you need to double check all mandatory fields in the requirements(in the CAD). If some of them are absent developer have to check them in the 'register.js' script(site level script). Because some fields can be ejected from the 'initialize.js' script(campaign level script) and injected to the 'register.js' script(site level script) to avoid code re-usage from campaign to campaign(e.g. 'Account ID'). If all necessary fields are presented you have to take an appropriate template and fill in the `[FIELDS]` by your data. 

Fields usually included into the 'register.js' script: `[ACCOUNT]`, `[SCOPE]` \*

Typical mandatory fields: `[ACCOUNT]`, `[EXPERIENCE]` \*

\* Each client can have his own tracking configuration and list of required fields. Provided templates describe common cases.


##List of integrations##

* [Google Analytics](#markdown-header-google-analytics)
* [Universal Analytics](#markdown-header-universal-analytics)
* [Core Metrics](#markdown-header-core-metrics)
* [AT-Internet](#markdown-header-at-internet)
* [Adobe Analytics (Omniture, SiteCatalyst)](#markdown-header-adobe-analytics-omniture-sitecatalyst)
* [ClickTale](#markdown-header-clicktale)
* [SessionCam](#markdown-header-sessioncam)
* [iJento](#markdown-header-ijento)

--------------------

##Google Analytics##


> **Given:** user generated into the test

> **When:** test page loaded

> **Then:** GA debugger(browser extension) logs into the console the request with the `[CUSTOM_VAR][X]` slot which is contains the `[LABEL]` property equals to `[TEST_ID]`, `[VALUE]` – `[EXPERIENCE]` and `[SCOPE]` – `[Y]`
> 
>
>> **Where:** 

>> `[CUSTOM_VAR]` - slot label name (e.g. 'Custom Var')

>> `[X]` - slot number(e.g. '4')

>> `[LABEL]` - slot label name(*optional*, e.g 'label')

>> `[TEST_ID]` - campaign label(*optional*, e.g. 'MAX-30') 

>> `[VALUE]` - slot label name(*optional*, e.g. 'value')

>> `[EXPERIENCE]` -  campaign generation string(e.g. 'T30_Anschlusspreis=a_anschpreis:Default')

>> `[SCOPE]` - slot label name(*optional*, e.g. 'scope')

>> `[Y]` - scope number(*optional*, e.g. '1')


###Screenshot example:###

![2015-10-19_1150.png](https://bitbucket.org/repo/qqKXx7/images/1650032308-2015-10-19_1150.png)

**QA Notes:**

* To filter requests to the GA by the URL you can use `__utm` entry

--------------------

##Universal Analytics##


> **Given:** user generated into the test

> **When:** test page loaded

> **Then:** GA debugger(browser extension) logs into the console the request with the `[DIMENSION][X]` slot equals to the `[EXPERIENCE]`
> 
> 
>> **Where:** 

>> `[DIMENSION]` - slot label name (e.g. 'dimension') 

>> `[X]` - slot number(e.g. '13')

>> `[EXPERIENCE]` -  campaign generation string(e.g. 'T29_eResale=a_layoutt29:a2_brandtrans')


###Screenshot example:###

![3rdAC.png](https://bitbucket.org/repo/qqKXx7/images/503631751-3rdAC.png)

**QA Notes:**

* To filter requests to the UA by the URL you can use `collect` entry

--------------------

##Core Metrics##


> **Given:** user generated into the test

> **When:** test page loaded

> **Then:** Coremetrics Tag Monitor(browser extension) logs the entry with the next properties:

> * for 'Element' tag  `[Element ID]` slot equals to the `[EXPERIENCE]` and `[Element Category]` - `[TEST_NAME]`

###Screenshot example:###

![1.png](https://bitbucket.org/repo/qqKXx7/images/1857917526-1.png)

> * for 'Page View' and 'Product View' tags  `[Page ID]` slot equals to the `[EXPERIENCE]` and `[Category ID]` - `[TEST_NAME]`

###Screenshot example:###

![2.png](https://bitbucket.org/repo/qqKXx7/images/1900000591-2.png)

> 
>> **Where:** 

>> `[Element ID], [Page ID]` - slot name

>> `[TEST_NAME]` -  campaign name(e.g. 'T47_FinishingTouches')

>> `[EXPERIENCE]` -  campaign generation string(e.g. 'a_ftouch:a2_finishtouch')


--------------------


##AT-Internet##


> **Given:** user generated into the test

> **When:** test page loaded

> **Then:** Browser have to register GET request with the parameter `[ABMVC]` equals to the `[EXPERIENCE]` and `[ABMV][X]` equals to the `[ELEMENT_VARIANT]`
> 
> 
>> **Where:** 


>> `[ABMVC]` - parameter name(e.g. 'abmvc')

>> `[EXPERIENCE]` -  campaign generation string(e.g. '[T01_Home]-1-[a_layout:Default]')

>> `[ABMV]` - parameter name(e.g. 'abmv') 

>> `[X]` - parameter number, equals to the element's order(e.g. '1')

>> `[ELEMENT_VARIANT]` - combination of the element and generated variant(e.g. '[a_layout]-[Default]')


###Screenshot example:###

![at-internet.png](https://bitbucket.org/repo/qqKXx7/images/860990169-at-internet.png)

**QA Notes:**

* To filter requests to the ATI by the URL you can use `hit.xiti` entry

--------------------

##Adobe Analytics (Omniture, SiteCatalyst)##


> **Given:** user generated into the test

> **When:** test page loaded

> **Then:** Adobe Analytics debugger(browser extension) logs into the console the request with the `[eVar][X]` slot equals to the `[EXPERIENCE]`
> 
> 
>> **Where:** 

>> `[eVar]` - slot label name (e.g. 'eVar') 

>> `[X]` - slot number(e.g. '50')

>> `[EXPERIENCE]` -  campaign generation string(e.g. 'T29_eResale=a_layoutt29:a2_brandtrans')


###Screenshot example:###

![sc.png](https://bitbucket.org/repo/qqKXx7/images/2805455214-sc.png)

* Omniture requests also can be traced by the [DigitalPulse Debugger](https://helpx.adobe.com/analytics/using/digitalpulse-debugger.html)

--------------------

##ClickTale##


> **Given:** user generated into the test

> **When:** test page loaded

> **Then:** Browser have to register GET request with the parameter `[X]` which contains the `[EXPERIENCE]`
> 
> 
>> **Where:** 

>> `[X]` - parameter name(usually it has randomly generated name, e.g. 'A1df3DFla')

>> `[EXPERIENCE]` -  campaign generation string(e.g. 'T30_RealSpeed=a_realspeed:a2_smallrealspeed')


###Screenshot example:###

![clicktale-2.png](https://bitbucket.org/repo/qqKXx7/images/31763802-clicktale-2.png)

**QA Notes:**

* To check the availability of the ClickTale API on the page you need to add `ct=enable,debug` parameter to the URL
* To check the initialization of the MM API for an appropriate campaign you need to match it's `[EXPERIENCE]` in the `window.EventStr` variable
* To filter record requests to the ClickTale by the URL you can use `clicktale.net` entry
* To check an appearance of the new entry in the ClickTale UI follow the [manual](https://bitbucket.org/mm-global-se/if-clicktale#markdown-header-clicktale-ui)

--------------------

##SessionCam##


> **Given:** user generated into the test

> **When:** test page loaded

> **Then:** Browser have to register POST request with the parameter `[data]` which contains the `[EXPERIENCE]`
> 
> 
>> **Where:** 

>> `[data]` - parameter name(e.g. 'data')

>> `[EXPERIENCE]` -  campaign generation string(e.g. 'T04_HOMEPAGEREDESIGN=A_HOMEPAGET4:A3_REDESIGN')


###Screenshot example:###

![sessioncam.png](https://bitbucket.org/repo/qqKXx7/images/476867106-sessioncam.png)

**QA Notes:**

* To check the initialization of the MM API for an appropriate campaign you need to match it's `[EXPERIENCE]` in the `window.sessioncamConfiguration.customDataObjects` variable - it should contain campaign info object
* To filter record requests to the SessionCam by the URL you can use `SaveEvents` entry
* To check an appearance of the new entry in the SessionCam UI follow the [manual](https://bitbucket.org/mm-global-se/if-sessioncam#markdown-header-qa)

--------------------

##iJento##


> **Given:** user generated into the test

> **When:** test page loaded

> **Then:** Browser have to register GET request with the parameter `[X]` which contains the `[EXPERIENCE]` base64 format encoded
> 
> 
>> **Where:** 

>> `[X]` - parameter name(e.g. 'd')

>> `[EXPERIENCE]` -  campaign generation string(e.g. 'T04_HOMEPAGEREDESIGN=A_HOMEPAGET4:A3_REDESIGN')


###Screenshot example:###

![iJento1.png](https://bitbucket.org/repo/qqKXx7/images/2529095385-iJento1.png)

**QA Notes:**

* To filter record requests to the iJentoby the URL you can use `track.gif` entry
* To check parameters of the request follow the [manual](https://bitbucket.org/mm-global-se/if-ijento#markdown-header-qa)

![iJento.png](https://bitbucket.org/repo/qqKXx7/images/2640935210-iJento.png)

* To decode the request parameters you can use any base64 decoder(e.g. [base64decode.org](https://www.base64decode.org/)):

1.Copy value of the `d` parameter

![iJento1.png](https://bitbucket.org/repo/qqKXx7/images/1964804879-iJento1.png)

2.Paste it into the decoding field, select output encoding(usually `utf-8` or `cp1256`) and you will get the value in the readable format

![iJento2.png](https://bitbucket.org/repo/qqKXx7/images/1471550718-iJento2.png)

--------------------